#Find Readme at: https://gitlab.com/Arovei/Porkbun_DDNS_Powershell
#Replace $sak and $apk with your keys from Porkbun; replace domain, type, and subdomain with whichever domain you are looking to update
#Porkbun Secret Key
$sak = "PUT_SECRET_KEY_HERE"
#Porkbun API Key
$apk = "PUT_API_KEY_HERE"
$domain = "PUT_DOMAIN_NAME_HERE"
$type = "PUT_SUBDOMAIN_TYPE_HERE"
$subdomain = "PUT_SUBDOMAIN_NAME_HERE"
#Porkbun does not support TTL lower than 600 (10 minutes)
$ttl = "600"
#Change this to alter where update logs are saved
#Make sure nothing else uses this folder, as it auto-deletes files older than 30 days
$LogPath = "C:\DNSLogs"
#End editable section

$script:UseBasic = @{UseBasicParsing=$true}

#Prep Log
$LogDate = Get-Date
$LogFileName = "PorkBunDNSUpdateLog_" + $LogDate.Month + $LogDate.Day + $LogDate.Year + "_" + $LogDate.Hour + $LogDate.Minute + $LogDate.Second + ".txt"

#Begin Log
Start-Transcript -Path ("C:\DNSLogs\" + $LogFileName)

#Get current WAN IP
#https://porkbun.com/api/json/v3/ping
$RequestBody = "{`"secretapikey`": `"$sak`", `"apikey`": `"$apk`"}"
$queryParams = @{
        Uri = "https://porkbun.com/api/json/v3/ping"
        Method = 'POST'
        Body = $RequestBody
        ErrorAction = 'Stop'
    }
$response = Invoke-RestMethod @queryParams @script:UseBasic
$wip = $response.yourIp
echo "WAN IP: $wip"

#Get current Domain IP
#https://porkbun.com/api/json/v3/dns/retrieveByNameType/DOMAIN/TYPE/[SUBDOMAIN]
$queryParams = @{
        Uri = "https://porkbun.com/api/json/v3/dns/retrieveByNameType/$domain/$type/$subdomain"
        Method = 'POST'
        Body = $RequestBody
        ErrorAction = 'Stop'
    }
$response = Invoke-RestMethod @queryParams @script:UseBasic
$dip = $response.records.content
echo "Domain IP: $dip"

#Compare current WAN IP with domain IP
#If they do not equal each other, update the Domain IP with the WAN IP
if ($wip -ne $dip) {
	#Build request body to update IP
	$RequestBody = "{`"secretapikey`": `"$sak`", `"apikey`": `"$apk`", `"content`": `"$wip`", `"ttl`": `"$ttl`"}"

	#Build full request to send
	#https://porkbun.com/api/json/v3/dns/editByNameType/DOMAIN/TYPE/[SUBDOMAIN]
	$queryParams = @{
			Uri = "https://porkbun.com/api/json/v3/dns/editByNameType/$domain/$type/$subdomain"
			Method = 'POST'
			Body = $RequestBody
			ErrorAction = 'Stop'
		}
	#Send request
	$response = Invoke-RestMethod @queryParams @script:UseBasic
	echo $response
}
else {
	echo "No difference in IP, skipping update push."
}

#Clean up old log files
Get-ChildItem -Path $LogPath | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-30)} | Remove-Item -Verbose

Stop-Transcript
