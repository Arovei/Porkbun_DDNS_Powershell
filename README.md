# Porkbun Dynamic DNS Updater

PBDDNS is a Powershell script that is designed to run using Windows Task Scheduler. It will update the specified IP of the subdomain. Originally built off [Jordan Tucker's Update-GoogleDynamicDns](https://github.com/jordanbtucker/Update-GoogleDynamicDns) script that i was using for years until Google sold their domains to Squarespace.

It's not perfect, but it works.

## Installation

Put the file somewhere on your system (not the same place as the logs, I personally use C:\DNSUpdate) then:
1. Edit the top of the file with your own domain information
2. With Windows Task Scheduler click "Task Scheduler Library" on the left
3. On the right, click "Create Basic Task...". 
4. Give it a name and description
5. Set the trigger to however often you want it to fire (mine is daily at 2 pm reoccurring every day)
6. Set action to "Start a program"
7. Select the file you downloaded/created
8. Check the box for "Open the Properties dialog" then click Finish
9. Click "Change User or Group", type SYSTEM in the box, then accept it
10. Change the radio button to "Run whether user is logged on or not"
11. Check the box for "Run with highest privileges"
12. Go to the trigger tab and edit the Daily trigger
13. Check the box for "Repeat task every:" and set it to 15 minutes for a duration of Indefinitely (you can set it for less if you want but keep in mind it will be hitting Porkbun's API servers every time it fires)
14. In the Settings tab, check "Run task as soon as possible after a scheduled start is missed" and uncheck "Stop the task if it runs longer than"


## Usage

Edit the top section of the file with your Secret Key, API Key, Domain, Subdomain Type, Subdomain, and change where the log outputs to if desired.

Follow instructions on [this Porkbun page](https://kb.porkbun.com/article/190-getting-started-with-the-porkbun-api) to get get your API keys and enable API access for your domain.
```powershell
#Replace $sak and $apk with your keys from Porkbun; replace domain, type, and subdomain with whichever domain you are looking to update
#Porkbun Secret Key
$sak = "PUT_SECRET_KEY_HERE"
#Porkbun API Key
$apk = "PUT_API_KEY_HERE"
$domain = "PUT_DOMAIN_NAME_HERE"
$type = "PUT_SUBDOMAIN_TYPE_HERE"
$subdomain = "PUT_SUBDOMAIN_NAME_HERE"
#Porkbun does not support TTL lower than 600 (10 minutes)
$ttl = "600"
#Change this to alter where update logs are saved
#Make sure nothing else uses this folder, as it auto-deletes files older than 30 days
$LogPath = "C:\DNSLogs"
#End editable section
```
